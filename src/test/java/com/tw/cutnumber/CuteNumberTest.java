package com.tw.cutnumber;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CuteNumberTest {

    @Test
    void should_be_greater_than_10() {
        CuteNumber cuteNumber = new CuteNumber();
        assertTrue(cuteNumber.isLargestNumberGreaterThan10(List.of(12)));
        assertTrue(cuteNumber.isLargestNumberGreaterThan10(List.of(6, 15, 9, 59, 12)));
    }

    @Test
    void should_not_be_greater_than_10() {
        CuteNumber cuteNumber = new CuteNumber();
        assertFalse(cuteNumber.isLargestNumberGreaterThan10(List.of(10)));
        assertFalse(cuteNumber.isLargestNumberGreaterThan10(List.of(6, 7, 8, 19)));
    }

}
package com.tw.cutnumber;

import java.util.ArrayList;
import java.util.List;

public class CuteNumber {

    public boolean isLargestNumberGreaterThan10(List<Integer> numbers) {
        List<Integer> evenNumbers = findAllEvenNumbers(numbers);
        int largestNumber = findLargestNumber(evenNumbers);
        return isGreaterThan10(largestNumber);
    }

    List<Integer> findAllEvenNumbers(List<Integer> numbers) {
        List<Integer> evenNumbers = new ArrayList<>();
        for (Integer number : numbers) {
            if (number % 2 == 0) {
                evenNumbers.add(number);
            }
        }
        return evenNumbers;
    }


    int findLargestNumber(List<Integer> numbers) {
        int largestNumber = 0;
        for (Integer number : numbers) {
            if (number > largestNumber) {
                largestNumber = number;
            }
        }
        return largestNumber;
    }

    boolean isGreaterThan10(int number) {
        return number > 10;
    }
}

package com.tw.oop;

public class Vehicle {
    protected String name;
    protected int speed;

    protected Engine engine;

    public Vehicle(String name, Engine engine) {
        this.name = name;
        this.engine = engine;
    }

    public void speedUp() {
        speed += engine.accelerate();
        System.out.println(String.format("%s: speed up to %d km/h", name, speed));
    }
}

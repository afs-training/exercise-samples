package com.tw.oop;

public abstract class Engine {
    public abstract int accelerate();
}

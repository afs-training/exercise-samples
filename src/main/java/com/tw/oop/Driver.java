package com.tw.oop;

public class Driver {
    private Vehicle vehicle;

    public void drive(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void speedUp() {
        vehicle.speedUp();
    }
}

package com.tw.oop;

public class OOPClient {
    public static void main(String[] args) {
        Driver driver = new Driver();
        driver.drive(new Car("Super Cool Car", new GasolineHybridEngine()));
        driver.speedUp();

        driver.drive(new Trunk("Big Trunk", new GasolineEngine()));
        driver.speedUp();
    }
}

package com.tw.posmachine;

import java.util.ArrayList;
import java.util.List;

public class ItemsLoader {
    public static List<Item> loadItems() {
        Item item1 = new Item("ITEM000000", "Coca-Cola", 3d);
        Item item2 = new Item("ITEM000001", "Sprite", 3d);
        Item item3 = new Item("ITEM000004", "Battery", 2d);
        List<Item> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        items.add(item3);

        return items;
    }
}

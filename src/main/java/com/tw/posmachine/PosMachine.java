package com.tw.posmachine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = mapToReceiptItems(barcodes);
        double totalPrice = calculateTotalPrice(receiptItems);
        return generateReceipt(receiptItems, totalPrice);
    }

    private String generateReceipt(List<ReceiptItem> receiptItems, double totalPrice) {
        String itemsReceipt = renderItemsReceipt(receiptItems);
        return renderWholeReceipt(totalPrice, itemsReceipt);
    }

    private String renderWholeReceipt(double totalPrice, String itemsReceipt) {
        String template = "***<store earning no money>Receipt***\n" +
                "%s\n" +
                "----------------------\n" +
                "Total: %.0f (yuan)\n" +
                "**********************";
        return String.format(template, itemsReceipt, totalPrice);
    }

    public String renderItemsReceipt(List<ReceiptItem> receiptItems) {
        StringJoiner stringJoiner = new StringJoiner("\n");
        for (ReceiptItem receiptItem : receiptItems) {
            stringJoiner.add(receiptItem.state());
        }
        return stringJoiner.toString();
    }

    public double calculateTotalPrice(List<ReceiptItem> receiptItems) {
        double totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.totalPrice();
        }
        return totalPrice;
    }

    public List<ReceiptItem> mapToReceiptItems(List<String> barcodes) {
        List<Item> items = loadItems(barcodes);
        Map<String, ReceiptItem> result = new LinkedHashMap<>();
        for (Item item : items) {
            if (result.containsKey(item.getBarcode())) {
                result.get(item.getBarcode()).addItem();
            } else {
                result.put(item.getBarcode(), new ReceiptItem(item));
            }
        }
        return new ArrayList<>(result.values());
    }

    private List<Item> loadItems(List<String> barcodes) {
        HashMap<String, Item> items = new HashMap<>();
        for (Item loadItem : ItemsLoader.loadItems()) {
            items.put(loadItem.getBarcode(), loadItem);
        }
        List<Item> result = new ArrayList<>();
        for (String barcode : barcodes) {
            result.add(items.get(barcode));
        }
        return result;
    }
}

package com.tw.posmachine;

public class ReceiptItem {
    private final String name;

    private final double unitPrice;

    private int quantity;

    public ReceiptItem(Item item) {
        this.name = item.getName();
        this.unitPrice = item.getPrice();
        addItem();
    }

    public void addItem() {
        quantity++;
    }

    public double totalPrice() {
        return quantity * unitPrice;
    }

    public String state() {
        String result = "Name: %s, Quantity: %d, Unit price: %.0f (yuan), Subtotal: %.0f (yuan)";
        return String.format(result, name, quantity, unitPrice, totalPrice());
    }
}

package com.tw.multiplication;

import java.util.StringJoiner;

public class MultiplicationTable {
    public String create(int start, int end) {
        if (!isValid(start, end)) {
            return null;
        }
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number >= 1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringJoiner line = new StringJoiner("\n");
        for (int i = start; i <= end; i++) {
            line.add(generateLine(start, i));
        }
        return line.toString();
    }

    public String generateLine(int start, int row) {
        StringJoiner line = new StringJoiner("  ");
        for (int i = 0; i < row - (start - 1); i++) {
            line.add(generateSingleExpression(start + i, row));
        }
        return line.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand * multiplier;
    }

    public static void main(String[] args) {
        System.out.println(new MultiplicationTable().generateTable(1, 9));
    }
}
